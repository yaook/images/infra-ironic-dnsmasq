##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

FROM almalinux:8 AS ipxe-builder

RUN set -eux ; \
    yum install -y \
        git \
        gcc \
        binutils \
        make \
        perl \
        mtools \
        xz-devel ; \
    mkdir -p /tmp/ipxe ; \
    cd /tmp/ipxe ; \
    git clone --depth 1 --branch v1.21.1 https://github.com/ipxe/ipxe.git ; \
    cd ipxe/src ; \
    make bin-x86_64-efi/ipxe.efi ; \
    make bin-x86_64-pcbios/undionly.kpxe

FROM ubuntu:24.04 AS ipxe-arm64-builder

RUN set -eux ; \
    apt-get update ; \
    apt-get install -y \
        git \
        gcc \
        mtools \
        make \
        gcc \
        gcc-aarch64-linux-gnu ; \
    mkdir -p /tmp/ipxe ; \
    cd /tmp/ipxe ; \
    git clone https://github.com/ipxe/ipxe.git ; \
    cd ipxe ; \
    git checkout 56cc61a168820c7cbbe23418388129ec11699a8c ; \
    cd src ; \
    make CROSS=aarch64-linux-gnu- ARCH=arm64 bin-arm64-efi/snponly.efi

FROM debian:bookworm

# gettext-base for envsubst(1)
RUN set -eux ; \
    apt-get update ; \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-suggests dnsmasq grepcidr iproute2 gettext-base ; \
    apt-get clean ; \
    rm -rf -- /var/cache/apt/lists
COPY --from=ipxe-builder /tmp/ipxe/ipxe/src/bin-x86_64-efi/ipxe.efi /tftpboot/ipxe.efi
COPY --from=ipxe-builder /tmp/ipxe/ipxe/src/bin-x86_64-pcbios/undionly.kpxe /tftpboot/undionly.kpxe
COPY --from=ipxe-arm64-builder /tmp/ipxe/ipxe/src/bin-arm64-efi/snponly.efi /tftpboot/ipxe-arm64.efi
